# TSPI-D310T9362V1

#### 介绍
泰山派3.1寸屏幕扩展板资料

屏幕型号：D310T9362V1

触摸型号：CST128-A

硬件开源地址：https://oshwhub.com/fengmoxi/tai-shan-pai-3-1-cun-ping-mu-kuo-zhan-ban

不会改设备树（或懒得改）的可以在[发行版](https://gitee.com/fengmoxi/tspi-d310t9362v1/releases/)里找编译好的kernel和固件

#### 硬件简介
泰山派3.1寸 mipi屏扩展板，通过pogopin连接泰山派底部触点，实现PD供电、串口、RTC、扬声器、麦克风功能

串口芯片使用CH343，可以看作CH340芯片的技术革新，支持最高6Mbps串口波特率，一般情况下免驱（驱动下载：https://www.wch.cn/products/CH343.html）。

通过一根6pin同向0.5mm间距和一根31pin同向0.3mm间距FFC/FPC软排线与泰山派连接

RTC电池使用1.25座连接，请选购带线BIOS电池，注意线序！

扬声器使用1.25座连接，YZ-2520 侧发声腔体喇叭


#### 注意事项
务必关注淘宝厂家发货的触摸芯片是CST128-A，资料是FT5316，建议以本文提供的驱动为准。

通过对比发现淘宝厂家发货的触摸芯片有两种固件，一种I2C地址为0x48（预估为CST原版固件），另一种I2C地址为0x38。

 **建议先尝试使用0x38来驱动触摸，若无法使用再尝试使用0x48，可以在Linux下使用i2cdetect -y 1确定I2C地址。** 


#### 提供用于编译SDK的虚拟机OVF模板一份
链接：https://pan.baidu.com/s/101PCDCm3T8gQF80RWW18jA?pwd=TSPI
提取码：TSPI


#### Patch文件用法
```
// 将补丁文件下载到SDK同级目录
wget https://gitee.com/fengmoxi/tspi-d310t9362v1/raw/master/0x38/linux-kernel-D310T9362V1-0x38.patch
wget https://gitee.com/fengmoxi/tspi-d310t9362v1/raw/master/0x38/android-kernel-D310T9362V1-0x38.patch

// Linux SDK
patch -p1 -N -d tspi_linux_sdk < linux-kernel-D310T9362V1-0x38.patch

// Android SDK
patch -p1 -N -d tspi_android_sdk < android-kernel-D310T9362V1-0x38.patch

```

> 如遇到类似Hunk #1 FAILED at xxx(different line endings).的问题，请通过dos2unix对相应文件进行格式转码
```
sudo apt install dos2unix
dos2unix tspi_android_sdk/kernel/arch/arm64/boot/dts/rockchip/tspi-rk3566-dsi-v10.dtsi
dos2unix tspi_linux_sdk/kernel/arch/arm64/boot/dts/rockchip/tspi-rk3566-dsi-v10.dtsi
```

#### 触摸驱动简要说明
1. /kernel/drivers/input/touchscreen下添加cst128a_cust

2. 在/kernel/drivers/input/touchscreen/Makefile文件最后增加
`obj-$(CONFIG_TOUCHSCREEN_CST128A_CUST)    += cst128a_cust/`

3. 在/kernel/drivers/input/touchscreen/Kconfig文件最后endif上一行增加
`source "drivers/input/touchscreen/cst128a_cust/Kconfig"`

4. 在/kernel/arch/arm64/configs/rockchip_linux_defconfig文件中增加`CONFIG_TOUCHSCREEN_CST128A_CUST=y`，注释或删掉`CONFIG_TOUCHSCREEN_GSL3673=y`
